import pandas as pd
import pickle


class UpSellLGBClassifier:
    def __init__(self):
        self.model = None

    def load_model(self, filepath):
        with open(filepath, 'rb') as file:
            self.model = pickle.load(file)

    def predict_proba(self, data: pd.DataFrame):
        return self.model.predict_proba(data)[:, 1]
