import pandas as pd
import numpy as np


class RandomClassifier:
    def __int__(self, seed):
        np.random.seed(seed)
        pass

    def fit(self):
        pass

    @staticmethod
    def predict_proba(data: pd.DataFrame) -> np.ndarray:
        return np.random.rand(data.shape[0], 1)

    def predict(self, data: pd.DataFrame) -> np.ndarray:
        proba = self.predict_proba(data)
        pred = (proba > 0.5).astype(int)
        return pred
