import pandas as pd


class InputProcessor(object):
    def __init__(self, df: pd.DataFrame, nan_cols=None, numerical_features=None, categorical_features=None):
        self.nan_cols = nan_cols
        self.numerical_features = numerical_features
        self.categorical_features = categorical_features
        self.df = df

    def fill_missing_value(self) -> None:
        for col in self.nan_cols:
            self.df[col] = self.df[col].fillna(-999)

    def cast_categorical_type(self) -> None:
        for col in self.categorical_features:
            self.df[col] = self.df[col].astype('category')

    def process(self) -> pd.DataFrame:
        self.fill_missing_value()
        self.cast_categorical_type()
        return self.df[self.numerical_features+self.categorical_features]
