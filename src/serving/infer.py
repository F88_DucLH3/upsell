import pandas as pd
import numpy as np
from src.classifier.lightgbm import UpSellLGBClassifier
from src.pre_processing.input import InputProcessor
from config.data_config import DATA_CONFIG
import os
import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('-m', '--model_dir', type=str, required=True)
    parser.add_argument('-f', '--file_name', type=str, required=True)

    args = parser.parse_args()
    model_file = os.listdir(args.model_dir)
    models = []
    for f in model_file:
        model = UpSellLGBClassifier()
        model.load_model(args.model_dir + '/' + f)
        models.append(model)
    n_models = len(models)
    data = pd.read_csv(args.file_name)
    CUSTOMER_CODE_2 = data['CUSTOMER_CODE_2'].values
    input_processor = InputProcessor(df=data, nan_cols=DATA_CONFIG.NUMERICAL_FEATURES,
                                     numerical_features=DATA_CONFIG.NUMERICAL_FEATURES,
                                     categorical_features=DATA_CONFIG.CATEGORICAL_FEATURES)
    input_data = input_processor.process()
    preds = np.zeros(data.shape[0])
    for model in models:
        preds += model.predict_proba(input_data) / n_models
    print(min(preds))
    res = list(zip(CUSTOMER_CODE_2, preds))
    # print(sorted(res, key=lambda x: -x[-1]))
